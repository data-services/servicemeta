/**
 * Copyright (C) 2019-2020  The Software Heritage developers
 * See the AUTHORS file at the top-level directory of this distribution
 * License: GNU Affero General Public License version 3, or any later version
 * See top-level LICENSE file for more information
 */

"use strict";

const LOCAL_CONTEXT_PATH = "./context/servicemeta.jsonld";
const LOCAL_CONTEXT_URL = "local";
const SERVICEMETA_CONTEXTS = {
    "0.1": {
        path: "./context/servicemeta.jsonld",
        url: `${window.location.origin}/context/servicemeta.jsonld`
    }
}


const loadContextData = async () => {
    const [contextLocal, contextV01] =
        await Promise.all([
            fetch(LOCAL_CONTEXT_PATH).then(response => response.json()),
            fetch(SERVICEMETA_CONTEXTS["0.1"].path).then(response => response.json())
        ]);
    return {
        [LOCAL_CONTEXT_URL]: contextLocal,
        [SERVICEMETA_CONTEXTS["0.1"].url]: contextV01
    }
}

const getJsonldCustomLoader = contexts => {
    return url => {
        const xhrDocumentLoader = jsonld.documentLoaders.xhr();
        if (url in contexts) {
            return {
                contextUrl: null,
                document: contexts[url],
                documentUrl: url
            };
        }
        return xhrDocumentLoader(url);
    }
};

const initJsonldLoader = contexts => {
    jsonld.documentLoader = getJsonldCustomLoader(contexts);
};

function emptyToUndefined(v) {
    if (v == null || v == "")
        return undefined;
    else
        return v;
}

function getIfSet(query) {
    console.log(query, document)
    return emptyToUndefined(document.querySelector(query).value);
}

function setIfDefined(query, value) {
    if (value !== undefined) {
        document.querySelector(query).value = value;
    }
}

function toObject(fieldset) {
    const doc = {};
    fieldset.querySelectorAll(".actor input").forEach((input) => {
        doc[input.name] = input.value;
    })
    return doc;
}

// Names of servicemeta properties with a matching HTML field name
const directServicemetaFields = [
    'alternateName',
    'dateCreated',
    'dateModified',
    'description',
    'documentation',
    'url',
    'identifier',
    'name',
    'releaseNotes',
    'version',
    'copyrightYear',
];

const splitServicemetaFields = [
    ['keywords', ','],
    ['relatedLink', '\n'],
    ['inputFormat', ','],
    ['outputFormat', ',']
]

// Names of servicemeta properties with a matching HTML field name,
// in a Person object
const directPersonServicemetaFields = actorFieldsMap["person"];
const directOrganizationServicemetaFields = actorFieldsMap["organization"];

const directRoleServicemetaFields = [
    'roleName',
    'startDate',
    'endDate',
];

function generateShortOrg(affiliation) {
    if (affiliation !== undefined) {
        if (isUrl(affiliation)) {
            return {
                "@type": "Organization",
                "@id": affiliation,
            };
        }
        else {
            return {
                "@type": "Organization",
                "name": affiliation,
            };
        }
    }
    else {
        return undefined;
    }
}



function generateRole(id) {
    const doc = {
        "@type": "Role"
    };
    directRoleServicemetaFields.forEach(function (item, index) {
        doc[item] = getIfSet(`#${id} .${item}`);
    });
    return doc;
}

function generateRoles(idPrefix, person) {
    const roles = [];
    const roleNodes = document.querySelectorAll(`ul[id^=${idPrefix}_role_`);
    roleNodes.forEach(roleNode => {
        const role = generateRole(roleNode.id);
        role["schema:author"] = person; // Prefix with "schema:" to prevent it from expanding into a list
        roles.push(role);
    });
    return roles;
}

function generateActor(actObj) {
    /*
    for (const key of Object.entries(actObj)) {
        switch (key) {
            case "actorType":
                doc["@type"] = value;
                break;
            case "identifier":
                doc["@id"] = value;
                break;
            case "affiliation":
                doc["affiliation"] = generateShortOrg(value);
                break;
            default:
                doc[key] = value;
        }
    }
    */
    const type = actObj["actorType"];
    const doc = {
        '@type': capitalize(type),
    }
    var id = emptyToUndefined(actObj["identifier"]);
    if (id) {
        doc['@id'] = id;
    }
    actorFieldsMap[type].forEach(function (item) {
        doc[item] = emptyToUndefined(actObj[item]);
    });
    if (actObj["affiliation"] !== undefined && actObj["affiliation"] !== ""){
        doc["affiliation"] = generateShortOrg(actObj["affiliation"]);
    }
    return doc;

}

function generateActors(prefix) {
    var actObjList = [];
    var actFldstList = document.querySelector(`#${prefix}_container`).querySelectorAll("fieldset.actor");

    actFldstList.forEach((actor) => {
        actObjList.push(generateActor(toObject(actor)));
    })

    return actObjList;
}


async function buildExpandedJson() {
    var doc = {
        "@context": SERVICEMETA_CONTEXTS["0.1"].url,
        "@type": "WebApplication",
    };

    // Generate most fields
    directServicemetaFields.forEach(function (item, index) {
        doc[item] = getIfSet('#' + item)
    });

    // Generate simple fields parsed simply by splitting
    splitServicemetaFields.forEach(function (item, index) {
        const id = item[0];
        const separator = item[1];
        const value = getIfSet('#' + id);
        if (value !== undefined) {
            doc[id] = value.split(separator).map(trimSpaces);
        }
    });

    // Generate dynamic fields
    var authors = generateActors('author');
    if (authors.length > 0) {
        doc["author"] = authors;
    }
    var contributors = generateActors('contributor');
    if (contributors.length > 0) {
        doc["contributor"] = contributors;
    }

    return await jsonld.expand(doc);
}


async function generateServicemeta(servicemetaVersion = "0.1") {
    var inputForm = document.querySelector('#inputForm');
    var servicemetaText, errorHTML;

    if (inputForm.checkValidity()) {
        const expanded = await buildExpandedJson();
        const compacted = await jsonld.compact(expanded, SERVICEMETA_CONTEXTS[servicemetaVersion].url);
        servicemetaText = JSON.stringify(compacted, null, 4);
        errorHTML = "";
    }
    else {
        servicemetaText = "";
        errorHTML = "invalid input (see error above)";
        inputForm.reportValidity();
    }

    document.querySelector('#servicemetaText').innerText = servicemetaText;
    setError(errorHTML);


    // Run validator on the exported value, for extra validation.
    // If this finds a validation, it means there is a bug in our code (either
    // generation or validation), and the generation MUST NOT generate an
    // invalid servicemeta file, regardless of user input.
    if (servicemetaText && !validateDocument(JSON.parse(servicemetaText))) {
        alert('Bug detected! The data you wrote is correct; but for some reason, it seems we generated an invalid servicemeta.json. Please report this bug at https://gitlab.ebrains.eu/data-services/servicemeta/issues/new and copy-paste the generated servicemeta.json file. Thanks!');
    }

    if (servicemetaText) {
        // For restoring the form state on page reload
        localStorage.setItem('servicemetaText', servicemetaText);
    }
}

// Imports a single field (name or @id) from an Organization.
function importShortOrg(fieldName, doc) {
    if (doc !== undefined) {
        // Use @id if set, else use name
        setIfDefined(fieldName, doc["name"]);
        setIfDefined(fieldName, getDocumentId(doc));
    }
}

function importPersons(prefix, legend, docs) {
    if (docs === undefined) {
        return;
    }

    docs.forEach(function (doc, index) {
        var personId = addPerson(prefix, legend);

        setIfDefined(`#${prefix}_${personId}_identifier`, getDocumentId(doc));
        directPersonServicemetaFields.forEach(function (item, index) {
            setIfDefined(`#${prefix}_${personId}_${item}`, doc[item]);
        });

        importShortOrg(`#${prefix}_${personId}_affiliation`, doc['affiliation'])
    })
}

async function importServicemeta() {
    var inputForm = document.querySelector('#inputForm');
    var doc = await parseAndValidateServicemeta(false);
    resetForm();

    directServicemetaFields.forEach(function (item, index) {
        setIfDefined('#' + item, doc[item]);
    });
    importShortOrg('#funder', doc["funder"]);

    // Import simple fields by joining on their separator
    splitServicemetaFields.forEach(function (item, index) {
        const id = item[0];
        const separator = item[1];
        let value = doc[id];
        if (value !== undefined) {
            if (Array.isArray(value)) {
                value = value.join(separator);
            }
            setIfDefined('#' + id, value);
        }
    });

    importPersons('author', 'Author', doc['author'])
    importPersons('contributor', 'Contributor', doc['contributor'])
}

function loadStateFromStorage() {
    var servicemetaText = localStorage.getItem('servicemetaText')
    if (servicemetaText) {
        document.querySelector('#servicemetaText').innerText = servicemetaText;
        importServicemeta();
    }
}
