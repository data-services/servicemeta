/**
 * Copyright (C) 2020  The Software Heritage developers
 * See the AUTHORS file at the top-level directory of this distribution
 * License: GNU Affero General Public License version 3, or any later version
 * See top-level LICENSE file for more information
 */

/*
 * Tests the basic features of the application.
 */

"use strict";

describe('JSON Generation', function() {
    beforeEach(function() {
        /* Clear the session storage, as it is used to restore field data;
         * and we don't want a test to load data from the previous test. */
        cy.window().then((win) => {
            win.sessionStorage.clear()
        })
        cy.visit('./index.html');
    });

    it('works just from the software name', function() {
        cy.get('#name').type('My Test Software');
        cy.get('#generateServicemeta').click();

        cy.get('#errorMessage').should('have.text', '');
        cy.get('#servicemetaText').then((elem) => JSON.parse(elem.text()))
            .should('deep.equal', {
                "@context": "https://gitlab.ebrains.eu/lauramble/servicemeta/-/raw/main/data/contexts/servicemeta.jsonld",
                "type": "WebApplication",
                "name": "My Test Software",
        });
    });

    it('works for new servicemeta terms in both versions', function() {
        cy.get('#name').type('My Test Software');
        cy.get('#contIntegration').type('https://test-ci.org/my-software');
        cy.get('#isSourceCodeOf').type('Bigger Application');
        cy.get('#reviewAspect').type('Some software aspect');
        cy.get('#reviewBody').type('Some review');

        cy.get('#generateServicemeta').click();
        cy.get('#errorMessage').should('have.text', '');
        cy.get('#servicemetaText').then((elem) => JSON.parse(elem.text()))
            .should('deep.equal', {
                "@context": "https://gitlab.ebrains.eu/lauramble/servicemeta/-/raw/main/data/contexts/servicemeta.jsonld",
                "type": "SoftwareSourceCode",
                "name": "My Test Software",
                "contIntegration": "https://test-ci.org/my-software",
                "codemeta:continuousIntegration": {
                    "id": "https://test-ci.org/my-software"
                },
                "codemeta:isSourceCodeOf": {
                    "id": "Bigger Application"
                },
                "schema:review": {
                    "type": "schema:Review",
                    "schema:reviewAspect": "Some software aspect",
                    "schema:reviewBody": "Some review"
                }
        });

        cy.get('#generateServicemetaV3').click();
        cy.get('#errorMessage').should('have.text', '');
        cy.get('#servicemetaText').then((elem) => JSON.parse(elem.text()))
            .should('deep.equal', {
                "@context": "https://w3id.org/codemeta/3.0",
                "type": "SoftwareSourceCode",
                "name": "My Test Software",
                "continuousIntegration": "https://test-ci.org/my-software",
                "codemeta:contIntegration": {
                    "id": "https://test-ci.org/my-software"
                },
                "isSourceCodeOf": "Bigger Application",
                "review": {
                    "type": "Review",
                    "reviewAspect": "Some software aspect",
                    "reviewBody": "Some review"
                }
        });
    });
});

describe('JSON Import', function() {
    it('works just from the software name', function() {
        cy.get('#servicemetaText').then((elem) =>
            elem.text(JSON.stringify({
                "@context": "https://gitlab.ebrains.eu/lauramble/servicemeta/-/raw/main/data/contexts/servicemeta.jsonld",
                "@type": "SoftwareSourceCode",
                "name": "My Test Software",
            }))
        );
        cy.get('#importServicemeta').click();

        cy.get('#name').should('have.value', 'My Test Software');
    });

    it('works with expanded document version', function () {
        cy.get('#servicemetaText').then((elem) =>
            elem.text(JSON.stringify({
                "http://schema.org/name": [
                    {
                        "@value": "My Test Software"
                    }
                ],
                "@type": [
                    "http://schema.org/SoftwareSourceCode"
                ]
            }))
        );
        cy.get('#importServicemeta').click();

        cy.get('#name').should('have.value', 'My Test Software');
    });

    it('errors on invalid type', function() {
        cy.get('#servicemetaText').then((elem) =>
            elem.text(JSON.stringify({
                "@context": "https://gitlab.ebrains.eu/lauramble/servicemeta/-/raw/main/data/contexts/servicemeta.jsonld",
                "@type": "foo",
                "name": "My Test Software",
            }))
        );
        cy.get('#importServicemeta').click();

        // Should still be imported as much as possible
        cy.get('#name').should('have.value', 'My Test Software');

        // But must display an error
        cy.get('#errorMessage').should('have.text', 'Wrong document type: must be "SoftwareSourceCode"/"SoftwareApplication", not "foo"');
    });

    it('allows singleton array as context', function() {
        cy.get('#servicemetaText').then((elem) =>
            elem.text(JSON.stringify({
                "@context": ["https://gitlab.ebrains.eu/lauramble/servicemeta/-/raw/main/data/contexts/servicemeta.jsonld"],
                "@type": "SoftwareSourceCode",
                "name": "My Test Software",
            }))
        );
        cy.get('#importServicemeta').click();

        cy.get('#name').should('have.value', 'My Test Software');
    });

});
